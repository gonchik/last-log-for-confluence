package com.tngtech.confluence.plugins.lastlog;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.log4j.Appender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;

import com.atlassian.confluence.logging.ConfluenceHomeLogAppender;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;


/**
 * Simple servlet which shows the log of current running Confluence installation based on script of Gerhard Mueller (see
 * at <a href="http://confluence.atlassian.com/display/DISC/lastlog+-+Get+last+part+of+atlassian-confluence+log"
 * >here</a>)
 */
public class LastLogServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(LastLogServlet.class);

    private static final long serialVersionUID = 1L;

    private static final String TEMPLATE_HTML = "<html><head><title>LastLog of Confluence</title></head><body>%s</body></html>";

    private PermissionManager permissionManager;

    private final List<String> messages = new ArrayList<String>();
    private final List<String> errors = new ArrayList<String>();


    public LastLogServlet(@ComponentImport PermissionManager permissionManager) {
        this.permissionManager = permissionManager;
    }


    /** empty constructor for compatibility to 2.10.4 */
    public LastLogServlet() {
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        messages.clear();
        errors.clear();

        if (hasPermission()) {
            // TODO could later be set by requests
            int maxReadLength = 100000;
            String appenderName = "confluencelog";

            // get the log4j log manager and ask it for the "" appender
            FileAppender appender = getAppender(appenderName);
            if (appender != null) {
                RandomAccessFile file = getFile(appender);
                if (file != null) {
                    byte[] byteArray = getFileContent(maxReadLength, file);
                    if (byteArray != null) {
                        outputLog(response, byteArray);
                        return;
                    }
                }
            }
        }
        outputAllMessages(response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }


    /** @return true if current user is administrator, otherwise false */
    boolean hasPermission() {
        ConfluenceUser user = AuthenticatedUserThreadLocal.get();
        if ((user != null) && (permissionManager.isConfluenceAdministrator(user))) {
            return true;
        }
        if (user == null) {
            LOGGER.warn("An user which was not logged in tried to use this plugin.");
            addMessage("You have to log in with an administrator account to see this page.");
        } else {
            LOGGER.warn("User '" + user.getFullName() + "' tried to use the plugin but had no permissions.");
            addMessage("You do not have permission to see this page. An administrator account required.");
        }
        return false;
    }


    /**
     * @param appenderName the name of the appender to be retrieved
     * @return the found rolling file appender or null if could not be retrieved
     */
    private FileAppender getAppender(String appenderName) {
        Appender appender = Logger.getRootLogger().getAppender(appenderName);


        // check appender type - ConfluenceHomeLogAppender and regular FileAppender are supported
        if (appender instanceof ConfluenceHomeLogAppender) {
            return getAppender((ConfluenceHomeLogAppender) appender);
        } else if (appender instanceof FileAppender) {
            return (FileAppender) appender;
        }

        if(appender==null){
            String msg = String.format("The logger for confluence file logging was not found.");
            addError(msg, null);
            return null;
        }

        String msg = String.format("Only '%s' and '%s' are supported but found '%s'",
                ConfluenceHomeLogAppender.class, FileAppender.class, appender.getClass());
        addError(msg, null);
        return null;
    }


    /**
     * @param appender the Confluence home log appender
     * @return the found rolling file appender or null if errors occurs
     */
    private FileAppender getAppender(ConfluenceHomeLogAppender appender) {
        String confHomeAppenderFieldName = "confluenceHomeAppender";

        // access private instance field
        try {
            Field privateField = appender.getClass().getDeclaredField(confHomeAppenderFieldName);
            privateField.setAccessible(true);
            return (FileAppender) privateField.get(appender);
        } catch (NoSuchFieldException e) {
            addError(String.format("Private appender field '%s' not found. Maybe API changed.", confHomeAppenderFieldName), e);
        } catch (IllegalAccessException e) {
            addError(String.format("Not possible to access private appender field '%s'.", confHomeAppenderFieldName), e);
        }
        return null;
    }


    /**
     * @param appender the appender containing the file to be retrieved
     * @return the retrieved random access file or null if error
     */
    RandomAccessFile getFile(FileAppender appender) {
        try { // create a random access file in read only mode
            return new RandomAccessFile(appender.getFile(), "r");
        } catch (FileNotFoundException e) {
            addError("File used by appender not found", e);
        }
        return null;
    }


    /**
     * @param maxReadLength the maximum bytes to be read
     * @param file the file which contains the content
     * @return the bytes retrieved from the file or {@code null} on error
     */
    byte[] getFileContent(int maxReadLength, RandomAccessFile file) {
        long fileLength;
        try {
            fileLength = file.length();
        } catch (IOException e) {
            addError("File length could not be accessed", e);
            return null;
        }

        // read set length
        long offset;
        int readLength;
        long start = fileLength - maxReadLength;
        if (start < 0) {
            offset = 0;
            readLength = (int) fileLength;
        } else {
            offset = start;
            readLength = maxReadLength;
        }

        try { // now seek to offset in file and read the part we need
            file.seek(offset);
        } catch (IOException e) {
            addError(String.format("I/O error '%s' while seeking for offset.", e.getMessage()), e);
            return null;
        }
        try {
            byte[] byteArray = new byte[readLength];
            int result = file.read(byteArray, 0, readLength);
            if (result < 0) {
                addMessage("No data could be read from log file because file was empty.");
            }
            return byteArray;
        } catch (IOException e) {
            addError(String.format("I/O error '%s' while read bytes from log file.", e.getMessage()), e);
        }
        return null;
    }


    void outputLog(HttpServletResponse response, byte[] byteArray) throws IOException {
        String body = "<p><h1 style=\"font-family: sans-serif;\">These are (up to) the last 100000 lines of the Confluence log:</h1><hr><pre>" + new String(byteArray) + "</pre></p>";

        response.setContentType("text/html");
        PrintWriter printWriter = response.getWriter();
        printWriter.print(String.format(TEMPLATE_HTML, body));
    }


    /**
     * @param response the output stream to write in
     * @throws IOException occurs if writer could not be retrieved
     */
    private void outputAllMessages(HttpServletResponse response) throws IOException {
        StringBuilder body = new StringBuilder("<p><b>Messages:</b><ul>");
        if (messages.isEmpty()) {
            body.append("<li>no messages available</li>");
        } else {
            for (String message : messages) {
                body.append("<li>").append(message).append("</li>");
            }
        }
        body.append("</ul></p><p><b>Errors:</b><ul>");
        if (errors.isEmpty()) {
            body.append("<li>no errors available</li>");
        } else {
            for (String error : errors) {
                body.append("<li>").append(error).append("</li>");
            }
        }
        body.append("</ul></p></pre>");

        response.setContentType("text/html");
        PrintWriter printWriter = response.getWriter();
        printWriter.print(String.format(TEMPLATE_HTML, body));
    }


    /** @param messageString the message */
    private void addMessage(String messageString) {
        messages.add(messageString);
    }


    /**
     * @param errorMessage the error message
     * @param exception the occurred exception
     */
    private void addError(String errorMessage, Throwable exception) {
        LOGGER.error(errorMessage, exception);
        errors.add(errorMessage);
    }


    /** @param permissionManager the PermissionManager to be injected */
    public void setPermissionManager(PermissionManager permissionManager) {
        this.permissionManager = permissionManager;
    }
}
