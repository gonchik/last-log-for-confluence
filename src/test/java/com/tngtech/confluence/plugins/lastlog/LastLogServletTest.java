package com.tngtech.confluence.plugins.lastlog;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

import java.io.File;
import java.io.PrintWriter;
import java.io.RandomAccessFile;

import javax.servlet.http.HttpServletResponse;

import com.atlassian.confluence.user.ConfluenceUser;
import org.apache.log4j.RollingFileAppender;
import org.mockito.ArgumentCaptor;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.atlassian.confluence.logging.ConfluenceHomeLogAppender;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;


/**
 * Testing {@link com.tngtech.confluence.plugins.lastlog.LastLogServlet}
 */
public class LastLogServletTest {

    @Test
    public void hasPermissionNoUserTest() throws Exception {
        LastLogServlet servlet = new LastLogServlet();
        assertEquals(servlet.hasPermission(), false, "incorrect permission");
    }


    @DataProvider(name = "hasPermissionDataProvider")
    public Object[][] hasPermissionDataProvider() {
        return new Object[][] {
                { Boolean.FALSE, Boolean.FALSE },
                { Boolean.TRUE, Boolean.TRUE },
            };
    }


    @Test(dataProvider = "hasPermissionDataProvider")
    public void hasPermissionTest(Boolean isAdmin, Boolean hasPermission) {
        ConfluenceUser user = mock(ConfluenceUser.class);
        PermissionManager permissionManager = mock(PermissionManager.class);

        AuthenticatedUserThreadLocal.set(user);
        when(Boolean.valueOf(permissionManager.isConfluenceAdministrator(user))).thenReturn(isAdmin);

        LastLogServlet servlet = new LastLogServlet();
        servlet.setPermissionManager(permissionManager);

        assertEquals(Boolean.valueOf(servlet.hasPermission()), hasPermission, "incorrect permission");
    }


    @Test
    public void getConfluenceHomeLogAppenderFieldTest() throws Exception {
        ConfluenceHomeLogAppender appender = new ConfluenceHomeLogAppender();
        assertNotNull(appender.getClass().getDeclaredField("confluenceHomeAppender"), "API may changed");
    }


    @Test
    public void getFileTest() throws Exception {
        RollingFileAppender appender = mock(RollingFileAppender.class);
        File tmpFile = File.createTempFile("lastlog-test", null);
        RandomAccessFile result = new RandomAccessFile(tmpFile, "r");
        when(appender.getFile()).thenReturn(tmpFile.getPath());

        LastLogServlet servlet = new LastLogServlet();
        assertEquals(servlet.getFile(appender).length(), result.length(), "incorrect length");
        for (int idx = 0; idx < result.length(); idx++) {
            assertEquals(servlet.getFile(appender).read(), result.read(), "incorrect byte at pos " + idx);
        }
    }


    @DataProvider(name = "getFileContentDataProvider")
    public Object[][] getFileContentDataProvider() {
        return new Object[][] {
                { Integer.valueOf(100), Integer.valueOf(100000) },
                { Integer.valueOf(100000), Integer.valueOf(100) },
                { Integer.valueOf(1000), Integer.valueOf(1000) },
            };
    }


    @Test(dataProvider = "getFileContentDataProvider")
    public void getFileContentTest(Integer fileLength, Integer maxReadLength) throws Exception {
        File tmp = File.createTempFile("lastlog-test", null);
        RandomAccessFile file = new RandomAccessFile(tmp, "rw");

        byte[] byteArray = new byte[fileLength.intValue()];
        for (int idx = 0; idx < fileLength.intValue(); idx++) {
            byteArray[idx] = ' ';
        }
        file.write(byteArray);

        byte[] bytes;
        if (maxReadLength.intValue() < fileLength.intValue()) {
            bytes = new byte[maxReadLength.intValue()];
        } else {
            bytes = new byte[fileLength.intValue()];
        }
        for (int idx = 0; idx < bytes.length; idx++) {
            bytes[idx] = ' ';
        }

        LastLogServlet servlet = new LastLogServlet();
        assertEquals(servlet.getFileContent(maxReadLength.intValue(), file), bytes, "incorrect file content");
    }

    @Test
    public void testOutputLog() throws Exception {
        String logMessage = "Hello World";

        PrintWriter printWriterMock = mock(PrintWriter.class);
        HttpServletResponse responseMock = mock(HttpServletResponse.class);

        ArgumentCaptor<String> stringCaptor = ArgumentCaptor.forClass(String.class);

        when(responseMock.getWriter()).thenReturn(printWriterMock);

        LastLogServlet servlet = new LastLogServlet();
        servlet.outputLog(responseMock, logMessage.getBytes());

        verify(responseMock).setContentType(anyString());
        verify(responseMock).getWriter();
        verify(printWriterMock).print(stringCaptor.capture());

        assertTrue(stringCaptor.getValue().contains(logMessage));
    }
}
